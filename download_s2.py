#!/usr/bin/python3

__author__ = "Sam Brooke"
__email__ = "sam@ucsb.edu"


import os
from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
from datetime import date
import argparse
import configparser
import json
import glob
import pandas as pd
import time
import sys

def main():
    parser = argparse.ArgumentParser(description='Download Sentinel 2 data by AOI and date')
    parser.add_argument('config', type=str, help='Location of configuration file')
    parser.add_argument('output_dir', type=str, help='Download location directory', default='./')

    cp = configparser.ConfigParser()

    cp.read(os.path.join('copernicus_credentials'))
    config = cp['copernicus']

    api = SentinelAPI(config['user'], config['password'], 'https://scihub.copernicus.eu/dhus')

    args = parser.parse_args() # Get CLI args


    with open(args.config, 'r') as config_file:

        process_config = json.loads(config_file.read())

        # search by polygon, time, and SciHub query keywords
        footprint = geojson_to_wkt(read_geojson(process_config['aoi']))
        products = api.query(footprint,
            platformname = 'Sentinel-2',
            date=(process_config['date1'], process_config['date2']),
            cloudcoverpercentage = (0, 5))

        # convert to Pandas DataFrame
        products_df = api.to_dataframe(products)

        
        csv_output = os.path.join(args.output_dir,process_config['name']+'_scene_list.csv')
        products_df.to_csv(csv_output)

        offline_df = products_df.copy(deep=False)
        
        for row in products_df.iterrows():

            if not os.path.exists(os.path.join(args.output_dir, row[1].title+'.zip')):

                if os.path.exists(os.path.join(process_config['output_ndwi'], row[1].filename)):
                    print('Directory already available at '+os.path.join(process_config['output_ndwi'], row[1].filename))
                else:
                    print('Checking product status...')

                    product_info = api.get_product_odata(row[1].uuid)

                    if product_info['Online']:
                        print('Downloading '+row[1].title+' ...')
                        time.sleep(1)
                        api.download(row[1].uuid, args.output_dir, checksum=True)
                    else:
                        print(row[1].title+' is offline, adding to wait list')
                        offline_df.append(row[1], ignore_index=True)
                        
            else:
                print('Zip file already available at '+os.path.join(args.output_dir, row[1].title+'.zip'))

        offline_output = os.path.join(args.output_dir,process_config['name']+'_offline_list.csv')
        offline_df.to_csv(offline_output)

if __name__ == '__main__':
	main()
