#!/usr/bin/python3

__author__ = "Sam Brooke"
__email__ = "sam@ucsb.edu"


import os
from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
import datetime
import argparse
import configparser
import json
import glob
import pandas as pd
import time
import sys
import numpy as np

def main():
    parser = argparse.ArgumentParser(description='Attempt to download Sentinel 2 requested from offline storage')
    parser.add_argument('config', type=str, help='Location of configuration file')
    parser.add_argument('waiting_scene_list', type=str, help='Location of LTE scenes requested and waiting to be downloaded')
    parser.add_argument('output_dir', type=str, help='Download location directory', default='./')

    cp = configparser.ConfigParser()

    cp.read(os.path.join('copernicus_credentials'))
    config = cp['copernicus']

    api = SentinelAPI(config['user'], config['password'], 'https://scihub.copernicus.eu/dhus')

    args = parser.parse_args() # Get CLI args

    seconds_in_day = 24 * 60 * 60
    cooldown = 600 # 10 minutes

    with open(args.config, 'r') as config_file:

        process_config = json.loads(config_file.read())

        waiting_list_df = pd.read_csv(args.waiting_scene_list)

        for idx, row in waiting_list_df.iterrows():

            if not os.path.exists(os.path.join(args.output_dir, row[1].title+'.zip')):

                if os.path.exists(os.path.join(process_config['output_ndwi'], row[1].title)):
                    print('Directory already available at '+os.path.join(process_config['output_ndwi'], row[1].title))
                else:

                    print('Requesting LTE retrieval...')

                    product_info = api.get_product_odata(row[1].uuid)
                    #
                    date_requested = pd.to_datetime(row[1]['request_time'])

                    date_now = datetime.datetime.now()

                    time_delta = date_now - date_requested
                    diff = divmod(time_delta.days * seconds_in_day + time_delta.seconds, 60)
                    hours_since_request = np.floor(diff[0]/60))

                    if hours_since_request > 24:

                        if product_info['Online']:
                            api.download(row[1].uuid, args.output_dir, checksum=True)

                            if os.path.exists(os.path.join(args.output_dir, row[1].title+'.zip')):
                                print('Download successfull!')
                                waiting_list_df.drop(idx)
                                waiting_list_df.to_csv(args.waiting_scene_list)

                                for i in range(cooldown,0,-1):
                                    sys.stdout.write(str(i)+' ')
                                    sys.stdout.flush()
                                    print('Cooldown before next LTE request...')
                                    time.sleep(1)

                        else:
                            print(row[1].title+' is not online yet...')
                            time.sleep(10)

            else:
                print('Zip file already available at '+os.path.join(args.output_dir, row[1].title+'.zip'))



if __name__ == '__main__':
	main()
