#!/opt/conda/bin/python3

__author__ = "Sam Brooke"
__email__ = "sam@tbotix.com"
__copyright__ = "Copyright 2020, Terrabotics Ltd."
__license__ = "Proprietary"

import os
from datetime import date
import rasterio
import argparse
import configparser
import json
import glob
from zipfile import ZipFile
from rasterio import features
import rasterio.mask
import numpy as np
import fiona

def crop_to_aoi(crop_output_dir, image, aoi_geometry, nodata=0):

    rcrop_name_path = crop_output_dir+'/'+os.path.basename(image)

    with rasterio.open(image) as source:

        out_meta = source.meta.copy()

        out_image, out_transform = rasterio.mask.mask(source, [aoi_geometry], crop=True, nodata=nodata)
        raster_data = source.read(1)

        out_meta.update({"driver": "GTiff",
                         "height": out_image.shape[1],
                         "width": out_image.shape[2],
                         "transform": out_transform,
                         "nodata": 0,
                         "crs": source.crs})

        raster_data = out_image

        raster_masked = np.ma.masked_invalid(raster_data, copy=True)

        with rasterio.open(rcrop_name_path, "w", **out_meta) as dest:
            dest.write(raster_masked)


def main():
    parser = argparse.ArgumentParser(description='Download Sentinel 2 data by AOI and date')
    parser.add_argument('config', type=str, help='Location of configuration file')
    parser.add_argument('output_dir', type=str, help='Download location directory', default='./')
    args = parser.parse_args() # Get CLI args

    with open(args.config, 'r') as config_file:

        process_config = json.loads(config_file.read())
        os.makedirs(process_config['output_ndwi'], exist_ok=True)
        os.makedirs(os.path.join(process_config['output_ndwi'], 'output'), exist_ok=True)

    s2_zips = glob.glob(os.path.join(process_config['output_raw'], '*.zip'))

    for zip in s2_zips:

        print('Processing '+zip[:-4])

        out_s2_unzip  = os.path.join(process_config['output_ndwi'], os.path.basename(zip[:-4]))+'.SAFE'

        if not os.path.exists(out_s2_unzip):
            print(out_s2_unzip+' does not exist, unzipping')
            # Create a ZipFile Object and load sample.zip in it
            with ZipFile(zip, 'r') as zipObj:
               # Extract all the contents of zip file in current directory
               zipObj.extractall(process_config['output_ndwi'])

        granule_dir = os.listdir(os.path.join(out_s2_unzip, 'GRANULE'))[0]

        B03_file = glob.glob(os.path.join(out_s2_unzip, 'GRANULE', granule_dir, 'IMG_DATA', '*B03.jp2'))[0]
        B08_file = glob.glob(os.path.join(out_s2_unzip, 'GRANULE', granule_dir, 'IMG_DATA', '*B08.jp2'))[0]

        ndwi_out = os.path.join(process_config['output_ndwi'], 'output', os.path.basename(B03_file[:-7])+'_NDWI.tiff')

        print(ndwi_out)

        with rasterio.open(B03_file, 'r') as B03_src:

            B03_band = B03_src.read(1)

            with rasterio.open(B08_file, 'r') as B08_src:

                B08_band = B08_src.read(1)

                NDWI = np.true_divide((B03_band - B08_band),(B03_band + B08_band))


                NDWI_water = NDWI<1

                profile = B08_src.profile.copy()
                profile.update(
                    dtype=np.byte,
                    driver='GTiff',
                    nodata=0
                )
                with rasterio.open(ndwi_out, 'w', **profile, nbits=1) as dst:
                    dst.write(NDWI_water.astype(np.byte), 1)

    # NDWI = (B03 - B08) / (B03 + B08)

if __name__ == '__main__':
	main()
