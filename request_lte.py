#!/usr/bin/python3

__author__ = "Sam Brooke"
__email__ = "sam@ucsb.edu"


import os
from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
import datetime
import argparse
import configparser
import json
import glob
import pandas as pd
import time
import sys

def main():
    parser = argparse.ArgumentParser(description='Download Sentinel 2 data by AOI and date')
    parser.add_argument('config', type=str, help='Location of configuration file')
    parser.add_argument('offline_scene_list', type=str, help='Location of offline scene list csv')
    parser.add_argument('output_dir', type=str, help='Download location directory', default='./')

    cp = configparser.ConfigParser()

    cp.read(os.path.join('copernicus_credentials'))
    config = cp['copernicus']

    api = SentinelAPI(config['user'], config['password'], 'https://scihub.copernicus.eu/dhus')

    args = parser.parse_args() # Get CLI args


    with open(args.config, 'r') as config_file:

        process_config = json.loads(config_file.read())

        waiting_list_df = pd.read_csv(args.offline_scene_list)

        lte_requests_path = os.path.join(args.output_dir,process_config['name']+'_lte_request_list.csv')

        if os.path.exists(lte_requests_path):
            lte_requests_df = pd.read_csv(lte_requests_path)
        else:
            with open(lte_requests_path, 'w') as f:
                f.write('uuid, title, request_time'+"\n")

        for row in waiting_list_df.iterrows():

            if not os.path.exists(os.path.join(args.output_dir, row[1].title+'.zip')):

                if os.path.exists(os.path.join(process_config['output_ndwi'], row[1].filename)):
                    print('Directory already available at '+os.path.join(process_config['output_ndwi'], row[1].filename))
                else:

                    print('Requesting LTE retrieval...')

                    product_info = api.get_product_odata(row[1].uuid)

                    check_if_dln = False
                    if product_info['Online']:
                        print('Product is online!')
                        check_if_dln = True
                    else:
                        if len(lte_requests_df[lte_requests_df['uuid'] == row[1].uuid]):
                            print(row[1].title+' is already in waiting list')
                            continue

                    api.download(row[1].uuid, args.output_dir, checksum=True)

                    if check_if_dln:
                        if os.path.exists(os.path.join(args.output_dir, row[1].title+'.zip')):
                            print('Download successfull!')
                            continue

                    with open(lte_requests_path, 'a') as f:
                        f.write(row[1].uuid+', '+row[1].title+', '+str(datetime.datetime.now())+"\n")

                    cooldown = 10 #seconds
                    for i in range(cooldown,0,-1):
                        sys.stdout.write(str(i)+' ')
                        sys.stdout.flush()
                        print('Cooldown before next LTE request...')
                        time.sleep(1)


            else:
                print('Zip file already available at '+os.path.join(args.output_dir, row[1].title+'.zip'))



if __name__ == '__main__':
	main()
